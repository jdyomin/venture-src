$(function() {
    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
        $('html').addClass('_safari');
    }

    var menuTrigger = $('.js-menu-trigger');

    menuTrigger.on('click', function() {
        $(this).closest('.js-header').toggleClass('opened');
        $('body').toggleClass('overflow');
    });

    var search = $('.js-search');

    search.on('click', function() {
        $(this).closest('form').toggleClass('expanded');
    });

    $(document).mouseup(function (e) {
        var searchForm = $('.search-form');

        if ((searchForm.has(e.target).length === 0) && searchForm.hasClass('expanded')) {
            searchForm.removeClass('expanded');
        }
    });

    var tabs = $('.js-tab-control');

    tabs.on('click', function() {
        if ($(window).width() < 1025) {
            var n = $(this).index();

            $('.js-tab-control').removeClass('active');
            $(this).addClass('active');

            var tabElements = $(this).closest('.js-tabs').find('.js-tab');

            tabElements.removeClass('active');
            tabElements.eq(n).addClass('active');
        }
    });

    if ($('.form__field-label._desktop').length) {
        var formLabel = $('.form__field');

        if ($(window).width() < 1025) {
            formLabel.on('focus', function() {
                $(this).prev('.form__field-label._desktop').fadeOut();
            });

            formLabel.on('blur', function() {
                if ($(this).val() === '') {
                    $(this).prev('.form__field-label._desktop').fadeIn();
                }
            });
        }
    }

    function navPosition(){
        var presentation = $('.presentation'),
            nav = $('.fixed-navigation');

        if (presentation.length) {
            $(window).on('scroll', function() {
                var top = $(this).scrollTop() + presentation.offset().top + presentation.outerHeight() / 2;
                if ($(this).scrollTop() > presentation.offset().top && top >= $(window).height() / 2) {
                    nav.addClass('scroll');
                } else {
                    nav.removeClass('scroll');
                }
            })
        }
    }

    navPosition();

    $(window).on('resize', function() {
        navPosition();
    });
});







